//1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando: $ mongoimport -d students -c grades < grades.json

//2) El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la colección con los siguientes comandos en la terminal de mongo: >use students; >db.grades.count() ¿Cuántos registros arrojo el comando count?
    //800

//3) Encuentra todas las calificaciones del estudiante con el id numero 4.
    db.grades.find({"student_id": 4});

//4) ¿Cuántos registros hay de tipo exam?
    //1
//5) ¿Cuántos registros hay de tipo homework?
    //2
//6) ¿Cuántos registros hay de tipo quiz?
    //1
//7) Elimina todas las calificaciones del estudiante con el id numero 3
    db.grades.deleteMany({"student_id": 3})

//8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?
    db.grades.find({"type": 'homework', "score": 75.29561445722392})
    //student_id = 9

//9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100
    db.grades.updateOne({"_id": ObjectId("50906d7fa3c412bb040eb591")}, {$set: {"score": 100}});

//10) A qué estudiante pertenece esta calificación.
    //estudiante con student_id = 6